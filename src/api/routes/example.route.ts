import exampleController from 'api/controllers/example.controller';
import { Routes } from 'api/interfaces/routes.interface';
import { Router } from 'express';

class ExampleRoute implements Routes {
  public path = '/';
  public router = Router();
  public exampleController = new exampleController();

  constructor() {
    this.initializeRoutes();
  }
  private initializeRoutes() {
    this.router.get(`${this.path}`, this.exampleController.example);

  }
}

export default ExampleRoute;
