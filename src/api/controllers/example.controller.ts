import { NextFunction, Request, Response } from 'express';
import ExampleService from 'api/services/example.services';

class exampleController {
  public exampleService = new ExampleService();

  public example = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> => {
    try {
      const result = await this.exampleService.exampleFunction()
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  };

}

export default exampleController;
