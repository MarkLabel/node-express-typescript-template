import ExampleRoute from 'api/routes/example.route';
import App from 'app';

const app = new App([
  new ExampleRoute(),
]);

app.listen();
//ถ้ามี routes ใหม่ ต้องประกาศเพิ่มเสมอ
